/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#include <string>
#include <vector>
#include <set>
#include <cstdio>
using namespace std;

#include "Play.h"

void showHelp();
char getUserInput();

int main(int argc, char **argv) {
    Game *pGame = new Game();
    pGame->level(0);
    pGame->show();//Show game layout

    char c;
    while ((c = getUserInput()) != 'Q') {
        switch (c) {
            case 't':
                pGame->start();
                break;
            case 'p':
                pGame->stop();
                break;
            case 'r':
                pGame->reset();
                break;
            case 'k'://up
                cout << "Who? ";
                c = getUserInput();
                pGame->moveComponent(c, UP);
                break;
            case 'j'://Down
                cout << "Who? ";
                c = getUserInput();
                pGame->moveComponent(c, DOWN);
                break;
            case 'h'://Left
                cout << "Who? ";
                c = getUserInput();
                pGame->moveComponent(c, LEFT);
                break;
            case 'l'://Right
                cout << "Who? ";
                c = getUserInput();
                pGame->moveComponent(c, RIGHT);
                break;
            case 'd'://Delete a component
                cout << "Remove who? ";
                c = getUserInput();
                pGame->removeComponent(c);
                break;
            case 'c'://Choose level
                cout << "Level range:0~" << pGame->getMaxLevel();
                level_t l = getUserInput();
                pGame->reset();
                pGame->level(l);
                pGame->start();
            case '?':
                showHelp();
                break;
        }
        //After every operation, show the result
        pGame->show();
        
    }

    delete pGame;
}
