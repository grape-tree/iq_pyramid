/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Functions to generate kinds of Shape.
 */
#include "ShapeFramework.h"

/*
 * Equilateral Triangle:
 * .
 * ..
 * ...
 * ....
 * .....
 */
static vector<Cordinator> genETriangle(unsigned int elements) {
    vector<Cordinator> vCor;
    unsigned int e = elements;
    unsigned int i = 1, j = 1;
    unsigned int k = 0;
    Cordinator cor;
    while (((1 + k)*k/2+1) < elements){
        for (j = 1; j <= i; j++) {
            cor.cor_vol = j;
            cor.cor_row = i;
            vCor.push_back(cor);
        }
        k = j;
        i++;
    }

    return vCor;
}

ShapeFramework genFramework(unsigned int element_numbers, ShapeType t) {
    ShapeFramework sf;
    sf.mElements = element_numbers;
    sf.mType = t;
    switch (t) {
        case EQUILATERALTRIANGLE:
            sf.mSF = genETriangle(element_numbers);
            break;
        default:
            break;
    }

    return sf;
}
