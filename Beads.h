/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Composition of several Beads.
 */
#ifndef BEADS_H
#define BEADS_H
#include "Bead.h"

class Beads {
public:
    virtual unsigned int getCount() = 0;
    virtual char getType() = 0;
    virtual Bead& getBead(pos_t pos) = 0;
    virtual void setPosition(pos_t where) = 0;
    virtual void remove() = 0;
    virtual void move(Direction di, unsigned int steps) = 0;
    virtual void rotate(Angle ag) = 0;
    virtual bool isIdle() = 0;
    virtual void update(unsigned int bead_index, bool status) = 0;
};

#endif
