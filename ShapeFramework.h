/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#ifndef SHAPEFRAMEWORK_H
#define SHAPEFRAMEWORK_H
#include "Shape.h"
#include "Cordinator.h"

class ShapeFramework;
ShapeFramework genFramework(unsigned int element_numbers, ShapeType t);

struct Cordinator {
    unsigned int cor_vol; 
    unsigned int cor_row;
};

class ShapeFramework {
    unsigned int mElements;
    ShapeType mType;
    
    vector<Cordinator> mSF;
public:
    unsigned int getIdxVol(unsigned int eleX) {
        return mSF.size() ? mSF[eleX].cor_vol : 0;
    }
    unsigned int getIdxRow(unsigned int eleX) {
        return mSF.size() ? mSF[eleX].cor_row : 0;
    }

    friend ShapeFramework genFramework(unsigned int element_numbers, ShapeType t);
}
#endif
