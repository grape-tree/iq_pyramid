/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#include <iostream>
#include <string>
#include "Utils.h"

using namespace std;

void print_triangle(int count, const set<char>& chs) {
    int i,j;
    int k = 0;
    while (((1 + k)*k/2+1) <= count) {
        for (j = 1; j <= i; j++) {
            cout << *chs++ << " ";
        }
        k = j;
        i++;
        cout << "\n";
    }
}
    
void IQPrint(int count, const set<char>& chs, ShapeType type) {
    switch (type) {
        case EQUILATERALTRIANGLE:
            print_triangle(count, chs);
            break;
        default:
            break;
    }
}
