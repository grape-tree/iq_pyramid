/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * a Holder can place a Bead.
 */
#ifndef BEADHOLDER_H
#define BEADHOLDER_H

class BeadHolder {
    pos_t idx_vol;
    pos_t idx_row;
    bool filled;
    char c;//default character of this BH is '*'
public:
    BeadHolder(pos_t iv, pos_t ir) 
        : idx_vol(iv), idx_row(ir), filled(false), c('*') {

    }

    pos_t getVolIdx() {
        return idx_vol;
    }
    pos_t getRowIdx() {
        return idx_row;
    }
    char getCh() {
        return c;
    }
    bool isFilled() {
        return filled;
    }
    void fillWith(char x) {
        c = x;
        filled = true;
    }
    void remove() {
        c = '*';
        filled = false;
    }
};
#endif
