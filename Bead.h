/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * A bead.
 */
#ifndef BEAD_H
#define BEAD_H

#include "Color.h"
#include "BType.h"

struct BeadInterface {
    pos_t left;
    pos_t right;
    pos_t up;
    pos_t down;
    set(pos_t l = 0, pos_t r = 0, pos_t u = 0, pos_t d = 0) {
        left = l;
        right = r;
        up = u;
        down = d;
    }
};

class Bead {
    pos_t position;//Relative position in a BeadsX
    BeadColor bc;
    BeadInterface bi;
    //properties when playing
    bool bconnected;
    Cordinator cor_holder;//BeadHolder's cordinator when place
public:
    BeadInterface  getBeadInterface() {
        return bi;
    }
    void setBeadInterface(BeadInterface i) {
        bi.left = i.left;
        bi.right = i.right;
        bi.up = i.up;
        bi.down = i.down;
    }
    void setColor(BeadColor c) {
        bc = c;
    }
    void setPosition(pos_t p) {
        position = p;
    }
    pos_t getPosition() {
        return position;
    }
    //Set status when playing
    void setStatus(bool c) {
        bconnected = c;
    }
    void setHolderCor(pos_t x, pos_t y) {
        cor_holder.cor_row = x;
        cor_holder.cor_vol = y;

    }
};
#endif
