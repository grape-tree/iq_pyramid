/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Composition of series BeadHolders.
 */
#ifndef BEADHOLDERBOARD_H
#define BEADHOLDERBOARD_H
#include <set>
#include "Shape.h"
#include "BeadHolder.h"

class BeadHolderBoard {
    unsigned int mBHNumbers;//Total numbers of BeadHolders
    ShapeType mBT;
    pos_t mMaxRows;
    pos_t mMaxVols;
    set<BeadHolder*> mBHset;
    /**
     * idx is the first Bead's position of x to put into Holder,
     * after this, put succedent Beads one by one.
     */
    enum { ACT_PUT, ACT_RMV, ACT_ROT, ACT_MOV };
    void setHolderStatus(pos_t idx, Beads* x, act_t action);

    void do_put(pos_t idx, Beads* x);
    void do_move(pos_t idx, Beads* x);
    void do_remove(pos_t idx, Beads* x);
    void do_rotate(pos_t idx, Beads* x);
public:
    BeadHolderBoard(unsigned int n, ShapeType bt);
    ~BeadHolderBoard();
    void show();
    bool put(Beads* x, pos_t where);
    bool remove(Beads* x);
    bool move(Beads* x, Direction di);
    bool rotate(Beads* x, Angle ag);
    
};
#endif
