/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * See below.
 */
#include "BeadsA.h"

const int BeadsA::count = 4;
const BeadColor BeadsA::bc = Gray;

//Static instance definition
BeadsA BeadsA::A;

/*Init sequence:Up->Down,Left->Right
 *  .
 *  .
 * ..
 */
void BeadsA::shape() {
    struct BeadInterface bi;

    Bead bd;
    bd.setColor(bc);

    //1# Bead
    bd.setPosition(1);
    bi.set(0, 4, 0, 0);
    bd.setBeadInterface(bi);
    bset.push_back(bd);
    //2# Bead
    bd.setPosition(2);
    bi.set(0, 0, 0, 3);
    bd.setBeadInterface(bi);
    bset.push_back(bd);
    //3# Bead
    bd.setPosition(3);
    bi.set(0, 0, 2, 4);
    bd.setBeadInterface(bi);
    bset.push_back(bd);
    //4# Bead
    bd.setPosition(4);
    bi.set(1, 0, 3, 0);
    bd.setBeadInterface(bi);
    bset.push_back(bd);
}

void BeadsA::remove() {
    //TODO:

    mIdle = true;
}

void BeadsA::move(Direction d, unsigned int steps) {
    //TODO:
    
}

void BeadsA::rotate(Angle ag) {
    //TODO

}

void BeadsA::setPosition(pos_t where) {
    
    mIdle = false;
}

void BeadsA::update(unsigned int bead_index, bool status) {
    bset[bead_index].setStatus(status);
}
