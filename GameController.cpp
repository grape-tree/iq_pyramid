/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#include "GameController.h"
#include "BeadsFactory.h"

using namespace std;

GameController::GameController() {
    mBHBoard = new BeadHolderBoard(56, EQUILATERALTRIANGLE);
    try {
        for (char x = 'A'; x <= 'L'; x++) {
            mElementsSet.push_back(BeadsFactory::getBeadsBytype(x));
        }    
    } catch (UnknownBeadsTypeException e) {
        cout << e.what() << endl;
    }
}

GameController::~GameController() {
    delete mBHBoard;
    set<Beads*>::iterator it = mElementsSet.begin();
    while (it != mElementsSet.end()) {
        delete *it++;
    }
}

void GameController::show() {
    mBHBoard->show();
}

void GameController::put(char x, pos_t where) {
    Beads* pBx = getElement(x);
    if (pBx->isIdle()) {
        if (true == mBHBoard->put(pBx, where)) {
            //Change BeadsX status
            pBx->setPosition(where);
        }
    }
    
}

void GameController::remove(char x) {
    Beads* pBx = getElement(x);
    if (!pBx->isIdle()) {
        if (true == mBHBoard->remove(pBx)) {
            pBx->remove();
        }
    }
}

void GameController::move(char x, Direction di) {
    Beads* pBx = getElement(x);
    if (!pBx->isIdle()) {
        if (true == mBHBoard->move(pBx, di)) {
            pBx->move(di, 1);//one time one step
        }
    }
}

void GameController::rotate(char x) {
    Beads* pBx = getElement(x);
    if (!pBx->isIdle()) {
        if (true == mBHBoard->rotate(pBx, CLOCK_90)) {
            pBx->rotate(CLOCK_90);//Fixed 90 clockwise
        }
    }
}
