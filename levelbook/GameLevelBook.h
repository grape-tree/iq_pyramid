/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#ifndef GAMELEVELBOOK_H
#define GAMELEVELBOOK_H
#include "BType.h"
#include "CreateGame.h"

class GameLevelBook {
    static GameLevelBook mLevelBook;
public:
    static GameLevelBook& getLevelBook() {
        return mLevelBook;
    }

    CreateGame& choose(level_t l) throw (UnknownGameLevelException);

    static level_t getMaxLevel();
}
#endif
