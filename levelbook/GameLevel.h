/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#ifndef CREATEGAMELEVEL_H
#define CREATEGAMELEVEL_H

#define MAX_LEVEL 8

#define DeclareGameLevelClass(x) \
class CreateGameLevel##x : public CreateGame { \
    static CreateGameLevel##x mLevel##x; \
    void preset(); \
    CreateGameLevel##x() : CreateGame() { \ 
        preset(); \ 
    } \
public: \
    static CreateGame& getLevel##x() { \
        return mLevel##x; \
    } \
    void show();
}

DeclareGameLevelClass(1);
DeclareGameLevelClass(2);
DeclareGameLevelClass(3);
DeclareGameLevelClass(4);
DeclareGameLevelClass(5);
DeclareGameLevelClass(6);
DeclareGameLevelClass(7);
DeclareGameLevelClass(8);

#if 0
class CreateGameLevel1 : public CreateGame {
    
class CreateGameLevel2 : public CreateGame {
    static CreateGameLevel2 mLevel2;
    void preset();
    CreateGameLevel2() : CreateGame() { 
        preset(); 
    }
public:
    static CreateGameLevel2& getLevel2() {
        return mLevel2;
    }
}
#endif
#endif
