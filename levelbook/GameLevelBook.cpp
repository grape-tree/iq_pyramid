/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#include "GameLevelBook.h"
#include "GameLevel.h"

typedef GameController& (*p_level_func)();

#define GenGameLevelFunc(x) GameLevel##x::getLevel##x

p_level_func genLevelArray[] = {
    GenGameLevelFunc(1),
    GenGameLevelFunc(2),
    GenGameLevelFunc(3),
    GenGameLevelFunc(4),
    GenGameLevelFunc(5),
    GenGameLevelFunc(6),
    GenGameLevelFunc(7),
    GenGameLevelFunc(8),
};

GameLevelBook GameLevelBook::mLevelBook;

#define MAX_LEVEL 8

GameController& GameLevelBook::choose(level_t l) throw (UnknownGameLevelException) {
    if (l > MAX_LEVEL)
        throw UnknownGameLevelException(l);

    return genLevelArray[l]();
}

level_t GameLevelBook::getMaxLevel() {
    return MAX_LEVEL;
}
