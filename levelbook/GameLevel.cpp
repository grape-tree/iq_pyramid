#include "CreateGame.h"

#define DefineStaticMember(x) CreateGameLevel##x CreateGameLevel##x::mLevel##x

DefineStaticMember(1);
DefineStaticMember(2);
DefineStaticMember(3);
DefineStaticMember(4);
DefineStaticMember(5);
DefineStaticMember(6);
DefineStaticMember(7);
DefineStaticMember(8);
/*
 *Game level 1:
 *-
 *-
 *-
 */
void CreateGameLevel1::preset() {
    putElement('A', 0);
    putElement(X, n);
    //...
}

void CreateGameLevel1::show() {
    
}

/*
 *Game level 2:
 *-
 *-
 *-
 */
void CreateGameLevel2::preset() {
    putElement('B', 0);
    putElement(X, n);
    //...
}

void CreateGameLevel2::show() {
    
}


