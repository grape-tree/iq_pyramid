/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Shape A class:Color:Gray, Count:4, Shape: .
 *                                           .
 *                                          ..
 */
#ifndef BEADSA_H
#define BEADSA_H
#include <set>
#include "Beads.h"
#include "Color.h"
#include "Direction.h"

class BeadsA : public Beads {
    static const int count;
    static const BeadColor c;
    set<Bead> bset;
    bool mIdle;
    BeadsA(const BeadsA&);
    void operator=(const BeadsA&);
    void shape();//Shape of BeadsA:relationship between Beads in A
    BeadsA() : mIdle(true) {
        shape();
    }
    static BeadsA A;
public:
    static Beads& getInstance() {
        return A;
    }
    virtual int getCount() { return count; }
    virtual char getType() { return 'A'; }
    virtual Bead& getBead(pos_t pos);
    virtual void remove();
    virtual void move(Direction di, unsigned int steps);
    virtual void rotate(Angle ag);
    virtual void setPosition(pos_t where);
    virtual bool isIdle() { return mIdle; }
    virtual void update(unsigned int bead_index, bool status);
};

#endif
