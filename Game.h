/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#ifndef PLAY_H
#define PLAY_H
#include "CreateGame.h"

class Game {
    CreateGame& mGameInstance;
    Game(const Game&);
    void operator=(const Game&);
    Beads* mActiveComponent;
    enum {GAME_RUNNING, GAME_STOPPED};
    status_t mGameStatus;//Running/Stopped
public:
    Game();
    ~Game(); 
    void level(level_t l = 0);
    level_t getMaxLevel();
    void start() { mGameStatus = GAME_RUNNING; }
    void stop() { mGameStatus = GAME_STOPPED; }

    void show();

    //Actions
    void reset();//reset to the status when level() was called
    void putComponent(char x, pos_t where);//put a component(BeadsX) onto Board 
    void moveComponent(char x, Direction di);//one time one step
    void removeComponent(char x);//only the components placed on Board can be removed 
    void rotateComponent(char x);
}
#endif
