/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 H .
 */
#include "ShapeFramework.h"
#include "BeadHolderBoard.h"

BeadHolderBoard::BeadHolderBoard(unsigned int n, BoardType bt) 
    : mBHNumbers(n), mBT(bt), mMaxVols(0), mMaxRows(0) {

    ShapeFramework shape = genFramework(mBHNumbers, bt);

    //Two dimension shape:from left to right, up to down.
    for (int i = 0; i < mBHNumbers; i++) {
        mBHset.push_back(new BeadHolder(shape.getVolIdx(i), shape.getRowIdx(i)));
        if (shape.getIdxVol(i) >= mMaxVols)
            mMaxVols = shape.getIdxVol(i);
        if (shape.getIdxRow(i) >= mMaxRows)
            mMaxRows = shape.getIdxRow(i);
    }
    
}

BeadHolderBoard::~BeadHolderBoard() {
    set<BeadHolder*>::iterator it;
    while (it != mBHset.end()) {
        delete *it;
        it++;
    }
}

pos_t BeadHolderBoard::getFirstValidIdlePosition(char x) {
    //TODO

}

pos_t BeadHolderBoard::calcNextValidIdlePosition(pos_t cur_pos, const Bead& b) {
    //TODO

}

void BeadHolderBoard::do_put(pos_t idx, Beads* x) {
    char c = x->getType();
    pos_t idle_pos = getFirstValidIdlePosition(c);
    unsigned int count = x->getCount();
    for (int i = 0; i < count; i++) {
        mBHset[idle_pos].fillWith(c);
        x->update(i, true);
        Bead& b = x->getBead(i);
        b.setHolderCor(mBHset[idle_pos].getVolIdx(), mBHset[idle_pos].getRowIdx());
        idle_pos = calcNextValidIdlePosition(idle_pos, b);

    }
}

void BeadHolderBoard::do_move(pos_t idx, Beads* x) {

}
void BeadHolderBoard::do_remove(pos_t idx, Beads* x) {

}
void BeadHolderBoard::do_rotate(pos_t idx, Beads* x) {

}

void BeadHolderBoard::setHolderStatus(pos_t idx, Beads& x, act_t action) {
    switch (action) {
        case ACT_PUT:
            do_put(idx, x);
            break;
        case ACT_MOV:
            do_move(idx, x);
            break;
        case ACT_RMV:
            do_remove(idx, x);
            break;
        case ACT_ROT:
            do_rotate(idx, x);
            break;
        default:
            break;
    }
    
}

void BeadHolderBoard::show() {
    set<char> cset;
    set<BeadHolder*>::iterator it = mBHset.begin();
    while (it != mBHset.end()) {
        cset.push_back((*it)->getCh());
        it++;
    }
    IQPrint(mBHNumbers, const set<char>& cs, mBT);

}

void BeadHolderBoard::put(Beads* x, pos_t where) {
    setHolderStatus(where, x, ACT_PUT);
}

bool remove(Beads* x);
    bool move(Beads* x, Direction di);
    bool rotate(Beads* x, Angle ag);

