/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Create a game. Components are:a Board, a set of BeadsX.
 */
#ifndef CREATEGAME_H
#define CREATEGAME_H
#include "BeadHolderBoard.h"
#include "Beads.h"

class GameController {
    BeadHolderBoard* mBHBoard;
    set<Beads*> mElementsSet;
    Beads* getElement(char x) {
        //'A' to 'L'
        return mElementsSet[x-'A'];
    };
public:
    GameController();
    ~GameController();
    BeadHolderBoard* getBoard() { return mBHBoard; }
    

    void put(Beads& x, pos_t where);
    void remove(char x);
    void move(char x, Direction di);
    void rotate(char x);

    void show();
}
#endif
