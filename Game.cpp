/**
 * @file
 * @author  name <mail>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * The time class represents a moment of time.
 */
#include <ncurses.h> //play with ncurses
#include "Game.h"

Game::Game() : mActiveComponent(NULL), mGameStatus(GAME_STOPPED) {
    //Init game environment:
    //-Draw a frame window
    //-Draw a board on left
    //-Draw 'A' to 'L' beads on right one by one
    //TODO
}

Game::~Game() {
    //TODO:
    //-Destroy game environment
}

void Game::level(level_t l = 0) {
    try {
        mGameInstance = GameLevelBook::getLevelBook().choose(l);
    } catch (UnknownGameLevelException e) {
        cout << e.what() << endl;
    }
}

level_t Game::getMaxLevel() {
    return GameLevelBook::getMaxLevel();
}

void Game::show() {
    mGameInstance.show();
}

void Game::reset() {
    mGameStatus = GAME_STOPPED;
    //TODO:
    
}

void Game::putComponent(char x, pos_t where) {
    if (mGameStatus == GAME_RUNNING)
        mGameInstance.put(x, where);
}

void Game::moveComponent(char x, Direction di) {
    if (mGameStatus == GAME_RUNNING)
        mGameInstance.move(x, di);
}

void Game::removeComponent(char x) {
    if (mGameStatus == GAME_RUNNING)
        mGameInstance.remove(x);
}

void Game::rotateComponent(char x) {
    if (mGameStatus == GAME_RUNNING)
        mGameInstance.rotate(x);
}

